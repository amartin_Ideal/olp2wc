package com.red_folder.phonegap.plugin.backgroundservice;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper
{
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "notificationDB";

    // Contacts table name
    private static final String TABLE_CONTACTS = "notificationTB";
    private static final String TABLE_CONTACTS2 = "storeTB";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_DEVICE_ID = "deviceID";
    private static final String KEY_MESG_REQ = "msgReq";
    private static final String KEY_MESG_RES = "msgResp";
    private static final String KEY_DATE_TIME = "dateTime";
    private static final String KEY_TOKEN = "token";
    private static final String KEY_STORE_ID ="storeID";
    private static final String KEY_CLIENT_ID ="clientID";
    private static final String KEY_COUNT ="count";
    private static final String KEY_API ="apiName";
    private static final String KEY_GROUP = "groupId";
    private static final String KEY_PRIVATEKEY ="privateKey";
    private static final String KEY_APITOKEN ="apiToken";
    private static final String KEY_CLIENTURL="clientUrl";
    private Context mContext;

    public DatabaseHandler(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        //3rd argument to be passed is CursorFactory instance
        this.mContext = context;
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        try {

            String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                    + KEY_ID + " INTEGER PRIMARY KEY," + KEY_DEVICE_ID + " TEXT,"
                    + KEY_MESG_REQ + " TEXT" + "," + KEY_MESG_RES + " TEXT," + KEY_DATE_TIME + " TEXT," + KEY_TOKEN + " TEXT," + KEY_STORE_ID + " INTEGER," + KEY_CLIENT_ID + " TEXT)";
            db.execSQL(CREATE_CONTACTS_TABLE);
            Log.e("onCreate", "onCreate call");

            String CREATE_CONTACTS_TABLE2 = "CREATE TABLE IF NOT EXISTS " + TABLE_CONTACTS2 + "("
                    + KEY_ID + " INTEGER PRIMARY KEY," + KEY_COUNT + " INTEGER " +"," + KEY_API + " TEXT," + KEY_GROUP + " INTEGER," + KEY_PRIVATEKEY + " TEXT," + KEY_APITOKEN + " TEXT," + KEY_CLIENTURL + " TEXT )";
            db.execSQL(CREATE_CONTACTS_TABLE2);
            Log.e("onCreate storeTB", "onCreate call");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }


    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);


        // Create tables again
        onCreate(db);


        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS2);
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new contact
    public void addContact(Notifications mesg) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        int stoteID=mesg.getStoreID();
        values.put("deviceID", mesg.getDeviceID()); // Contact Name
        values.put("msgReq", mesg.getmsgReq()); // Contact Phone

        values.put("msgResp", mesg.getmsgResp()); // Contact Name
        values.put("dateTime", mesg.getDateTime()); // Contact Phone
        values.put("token", mesg.getToken()); // Contact Phone
        values.put("storeID", stoteID); // Contact Phone

        values.put("clientID", mesg.getClientID()); // Contact Phone

        // Inserting Row
        long rowInserted= db.insert(TABLE_CONTACTS, null, values);

        if(rowInserted != -1) {

            Cursor cursor= db.rawQuery("SELECT msgResp FROM " + TABLE_CONTACTS + " ORDER BY id DESC LIMIT 1 ", null);
            if (cursor.moveToFirst()){
                while(!cursor.isAfterLast()){
                    String str = (cursor.getString(cursor.getColumnIndex("msgResp")));

                    Log.e("resp value", str);
                    cursor.moveToNext();
                }
            }
           // Cursor clientID = db.rawQuery("SELECT max(id) FROM " + TABLE_CONTACTS + " ", null);
           // clientID.moveToFirst();
           // int a= clientID.getCount();
            Log.e("inserted, row id", "onCreate call");
        }
        else
        Log.e("Something wrong", "onCreate call");

        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }


    public String isExist(int sID){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCount = db.rawQuery("SELECT count(*) FROM " + TABLE_CONTACTS + " where storeID = '" +sID + "'" , null);
        mCount.moveToFirst();
        int count= mCount.getInt(0);

        if(count >0)
        {
            Cursor clientID = db.rawQuery("SELECT clientID FROM " + TABLE_CONTACTS + " where storeID = '" +sID + "' AND clientID!=NULL ORDER BY id DESC LIMIT 1 ", null);
            clientID.moveToFirst();
            int a= clientID.getCount();

            if(a==0)
            {
                return "no record found";
            }
            else
            {
                mCount.moveToFirst();
                String value= clientID.getString(0);
                mCount.close();
                return value;
            }
        }
        else
        {
            return "no record found";
        }
    }


    public Cursor getStore(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor= db.rawQuery("SELECT count,apiName,groupId,privateKey,apiToken,clientUrl,email,phoneNumber,deviceID,devicePlatform FROM " + TABLE_CONTACTS2 , null);

         if (cursor.moveToFirst()){
                while(!cursor.isAfterLast()){
                   int storeId = Integer.parseInt(cursor.getString(cursor.getColumnIndex("count")));
                    // do what ever you want here

                    cursor.moveToNext();
                }
            }
            return cursor;
        };


    public int getStoreIndex(int locationID){
        SQLiteDatabase db = this.getReadableDatabase();
        int storeId=1;
        Cursor cursor= db.rawQuery("SELECT id FROM " + TABLE_CONTACTS2 + " where count= "+locationID+"" , null);
        if (cursor.moveToFirst()){
            while(!cursor.isAfterLast()){
                storeId = Integer.parseInt(cursor.getString(cursor.getColumnIndex("id")));
                cursor.moveToNext();
            }
        }
        cursor.close();
       // int storeId = Integer.parseInt(cursor.getString(cursor.getColumnIndex("id")));

int id=storeId;

        return storeId;
    };




    // Getting single contact
    Notifications getContact(int sID) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor= db.rawQuery("SELECT * FROM " + TABLE_CONTACTS + " where storeID = '" +sID + "'" , null);

     /*   if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();

            }
            cursor.close();
        }

         cursor = db.query(TABLE_CONTACTS, new String[] { KEY_ID,
                        KEY_DEVICE_ID, KEY_MESG_REQ,KEY_MESG_RES ,KEY_DATE_TIME,KEY_TOKEN,KEY_STORE_ID,KEY_CLIENT_ID}, KEY_ID + "=?",
                new String[] { String.valueOf(sID) }, null, null, null, null); */

        if (cursor != null)
            cursor.moveToFirst();

        Notifications contact = new Notifications(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(3),cursor.getString(4),cursor.getInt(5),cursor.getString(6));
        // return contact
        return contact;
    }

    // Getting All Contacts
    public List<Notifications> getAllContacts() {
        List<Notifications> contactList = new ArrayList<Notifications>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Notifications contact = new Notifications();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setDeviceID(cursor.getString(1));
                contact.setmsgReq(cursor.getString(2));
                contact.setmsgResp(cursor.getString(3));
                contact.setToken(cursor.getString(4));
                contact.setStoreID(cursor.getInt(5));
                contact.setClientID(cursor.getString(6));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }

    // Updating single contact
    public int updateContact(Notifications contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("KEY_DEVICE_ID", contact.getDeviceID()); // Contact Name
        values.put("KEY_MESG_REQ", contact.getmsgReq()); // Contact Phone

        values.put("msgResp", contact.getmsgResp()); // Contact Name
        values.put("dateTime", contact.getDateTime()); // Contact Phone
        values.put("dateTime", contact.getDateTime()); // Contact Phone
        values.put("token", contact.getToken()); // Contact Phone
        values.put("storeID", contact.getStoreID()); // Contact Phone

        values.put("clientID", contact.getClientID()); // Contact Phone

        // updating row
        return db.update(TABLE_CONTACTS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });
    }

    // Deleting single contact
    public void deleteContact(Notifications contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });
        db.close();
    }

    // Getting contacts Count
    public int getContactsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CONTACTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        // return count
        return cursor.getCount();
    }

}