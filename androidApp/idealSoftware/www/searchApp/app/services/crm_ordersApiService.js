// call all api methods using faye server
angular.module("app")
    .service("crmOrdersApi", ["$rootScope", "$cookieStore", "$timeout", "SharedService", "configDetailsProvider", function($rootScope, $cookieStore, $timeout, SharedService, configDetailsProvider) {
        var responseChannelPrefix;
        var responseChannelPrefixCrm;
        var clientID;
        var data;
        var evl;
        var client;
        var responseChannel;
        var isSubscribed = false;
        var handshake = "/meta/handshake";
        var connect = "/meta/connect";
        var subscribe = "/meta/subscribe";
        var validToken;
        var Logger;
        var time_last;
        var AuthenticateRequest;
        var apiToken;
        var apiKey;
        var apiStore;
        var groupId;
        var location_url;
        var firstConnection = true;
        var timeout;
        var siteId;
//        var locationId;
        var apiCrmStore;
        var waitTime = 120000;
        var waitTimeReq = "";
        var responseChannelCrm;

        try {

            // cal and read api_cookies
            //SharedService.writeApiCookie();
            groupId = configDetailsProvider.apiConnect.group;
            apiStore = configDetailsProvider.apiConnect.api;
            apiCrmStore = configDetailsProvider.apiConnect.apiCrm;
//            locationId = sessionStorage.getItem("locationId")

            if (sessionStorage.getItem("siteID") == null || sessionStorage.getItem("siteID") == "") {
                siteId = parseInt(SharedService.getData());
            } else {
                siteId = parseInt(sessionStorage.getItem("siteID"));
            }

            responseChannelPrefix = "/" + apiStore + "/" + groupId + "/" + siteId;
            responseChannelPrefixCrm="/" + "crm_orders" + "/" + groupId + "/" + "crmChannel";
            console.log(responseChannelPrefix);

            // Connect To Faye
            var client_url = configDetailsProvider.apiConnect.client_url;
            var start = new Date().getTime();
            client = new Faye.Client(client_url, {
                timeout: 120
            });
            client.connect();
            client.disable("WebSocket");

            Logger = {
                incoming: function(message, callback) {

                    console.log(" incoming: ", message);
                    if (message.channel == handshake && message.successful) {
                        var obj = JSON.parse(JSON.stringify(message));
                        clientID = (obj.clientId);

                        responseChannelCrm="/" + "crm_orders" + "/" + groupId + "/" + clientID + "/response";
                        console.log(responseChannel);
                        $rootScope.messageReceived = {
                            data: {
                                message: "connect"
                            }
                        };
                        $rootScope.$apply();
                        if (!isSubscribed) {
                            SubscribeIt(responseChannelCrm);
                        }
                    }
                    return callback(message);
                },
                outgoing: function(message, callback) {
                    if (message) {
                        console.log(" outgoing: ", message);
                        try {
                            evl = "";
                            var jsonString = JSON.stringify(message);
                            var json = message;
                            apiToken = configDetailsProvider.apiConnect.token;
                            apiStore = configDetailsProvider.apiConnect.api;

                            if (message.channel == subscribe) {
                                var salt = random128();
                                evl = {
                                    "api": 'crm_orders',
                                    "token": apiToken,
                                    "salt": Base64.encode(salt),
                                    "signature": createSignature(salt, jsonString),
                                    "message": subscribe,
                                    "data": Base64.encode(jsonString)
                                };
                                message.ext = evl;

                                console.log("envolope=   ======" + evl);
                                return callback(message)
                            };

                            if (json.data != "undefined" && json.data != null) {
                                var salt = random128();
                                var message1;

                                if ((json.data != null) && (json.data.v1 != null)) {
                                    message1 = json.data.message;
                                } else {
                                    console.log("[VR][API]Packet missing v1 section or v1.message element. \r\n" + json.data);
                                }

                                evl = {
                                    "api": 'crm_orders',
                                    "token": apiToken,
                                    "salt": Base64.encode(salt),
                                    "signature": createSignature(salt, jsonString),
                                    "message": message1,
                                    "data": Base64.encode(jsonString)
                                };

                                message.ext = evl;
                                message.data = json.data;
                            };
                        } catch (err) {
                            console.log(err.message);
                        }
                    }

                    callback(message);
                }
            }

            // call extension
            client.addExtension(Logger);

            function cancelTimer() {
                console.log("cancel timeout");
                $timeout.cancel(timeout);
            };

            function stopLoader() {
                $rootScope.ShowLoader = false;
                console.log("The server cannot or will not process the request");
                $rootScope.apiErrorMessage = "The server cannot or will not process the request";
            };

            function startTimer() {
                timeout = $timeout(function() {
                    console.log("finish timeout");
                    $rootScope.ShowLoader = false;
                    console.log("The server cannot or will not process the request");
                    $rootScope.apiErrorMessage = "The server cannot or will not process the request";
                }, waitTime);
            };

            //Subscribe here
            function SubscribeIt(rc) {
                var subscription = client.subscribe(rc, function(msg) {

                    console.log("subscribed data " + JSON.stringify(msg));
                    $rootScope.messageReceived = msg;
                    $rootScope.$apply();

                    //clear timer
                    if (msg.data.message == waitTimeReq) {
                       // alert(waitTimeReq);
                        cancelTimer();
                    }
                    // new_opportunityReq();

                }).then(function(msg) {
                    console.log("subscription successful!!");
                    console.log("subscribed");
                    isSubscribed = true;
                   // site_informationReq();
                   // list_categoriesReq();
                }, function(error) {
                    console.log("Error subscribing: " + error.message);
                });
            };

            // signature
            function createSignature(salt, json) {
                // api_key
                var api_key = configDetailsProvider.apiConnect.key;
                var signature = (SHA256(SHA256(SHA256(utf8.encode(json)) + utf8.encode(api_key)) + (salt)));
                return signature;
            };


            // guid
            function createGuid() {
                function _p8(s) {
                    var p = (Math.random().toString(16) + "000000000").substr(2, 8);
                    return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
                }
                return _p8() + _p8(true) + _p8(true) + _p8();
            };

            // currentdatetime
            function CreateDate() {
                var currentdate = new Date();
                var currentDateTime = currentdate.toISOString();
                return currentDateTime;
            };

         this.new_opportunityReq = function()
            {
            var data= sessionStorage["PayDepositDetails"];
            var newval =data;
            var cus1=(JSON.parse(newval));
           var data = new_opportunity(cus1.customerinfo, cus1.reference, cus1.employeeInfo,cus1.items,cus1.depositAmount, cus1.MinimumDepositAmount,cus1.proceType);
            var publication = client.publish(responseChannelPrefixCrm, data).then(function() {
                console.log("Successfully published!");
                waitTimeReq = "new_opportunity";
                startTimer();
            }, function(error) {
                console.log("Error publishing: " + error.message);
                stopLoader();
            });
            }

          function new_opportunity(cust, ref,emp,items,depositAmount, MinimumDepositAmount) {
           var messageid = createGuid();
                  var currenttimestamp = CreateDate();
                  var locationId = sessionStorage.getItem("locationId")
                  var birthDate;
                  var str = cust.birthdate;
                  if (str == "") {
                      birthDate = "";
                  } else {
                      var darr = str.split("/"); // ["29", "1", "2016"]
                      var dobj = new Date(parseInt(darr[2]), parseInt(darr[1]) - 1, parseInt(darr[0]));
                      birthDate = dobj.toISOString();
                  }
                  var data = {
                      "v1": {
                          "returnChannel": responseChannelCrm,
                          "messageID": messageid,
                          "locationID": locationId,
                          "depositCollected": parseFloat(MinimumDepositAmount),
                          //"customerID": string,  // --VR customer ID optional only if known
                          "person": {
                              //"peopleID": string, // --CRM PeopleID optional only if known
                              "firstName": cust.firstName,
                              "lastName": cust.lastName,
                              "middleInitial": cust.middleName,
                              "address": cust.address,
                              "city": cust.city,
                              "state": cust.state,
                              "zip": cust.zipCode,
                              "plusFour": cust.four,
                              "apt": cust.apt,
                              "dlNumber": cust.driverLicence,
                              "dlState": cust.dlState,
                              "birthDate": birthDate,
                              "homePhone": cust.homePhoneNumber,
                              "cellPhone": cust.cellPhoneNumber,
                              "workPhone": cust.workPhoneNumber,
                              "email": cust.email,
                              "references": [{
                                  "name": ref.refName,
                                  "address": ref.refAddress,
                                  "city": ref.refCity,
                                  "state": ref.refState,
                                  "zip": ref.refZipCode,
                                  "phone": ref.refPhone,
                                  "relation": ref.refRelation,
                              }],
                              "employer": [{
                                  "name": emp.employeeName,
                                  "address": emp.employeeAddress,
                                  "city": emp.employeeCity,
                                  "state": emp.employeeState,
                                  "zip": emp.employeeZipCode,
                                  "phone": emp.employeePhone,
                                  "frequency": emp.per,
                                  "rate": parseFloat(emp.employeeSalary),
                              }]
                          },
                          "items": [],
                          "currentTimeStamp": currenttimestamp
                      },
                      "message": "new_opportunity"
                  };

                        for (var i = 0; i < items.length; i++) {
                              var item = items[i];

                              console.log("card pcid= " + item.pcid);
                              console.log("modid= " + item.modid);
                              console.log("invid= " + item.id);

                              data.v1.items.push({
                                  "itemType": 2, // integer --1=pcid 2=modid 3=invid
                                  "pcid": item.pcid,
                                  "modid": item.modid,
                                  "invid": item.id, // list_inventory
                                  "term": item.term, // list_inventory
                                  "weeklyRate": parseFloat(item.weeklyRate), // list_inventory
                                  "semiRate": parseFloat(item.semiRate), // list_inventory
                                  "monthlyRate": parseFloat(item.monthlyRate), // list_inventory
                                  "price": parseFloat(item.price), // list_inventory
                              });
                          }

                  return data;
  }

            // random128
            function random128() {
                var result = "";
                for (var i = 0; i < 8; i++)
                    result += String.fromCharCode(Math.random() * 0x10000);
                return result;
            };
        } catch (e) {
            console.log(e.message);
            // errorMessage redirect to error page.
            //  $state.path("/error").search("ref", e.message);
            $rootScope.Loaded = true;
        }

    }]);