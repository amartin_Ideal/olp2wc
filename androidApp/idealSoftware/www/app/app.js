﻿//     var YourAppName = angular.module('app', []);
angular.module("app", ["ngRoute", "ngCookies", "sharedService"])
 .config(function($routeProvider, $locationProvider) {
  $routeProvider.
  when("/menu", {
   templateUrl: "views/hamburgurMenu.html",
   controller: "MenuCtrl",
  })
  .when("/chat/:siteID", {
   templateUrl: "views/charMedium.html",
   controller: "chatCtrl",
  })
  .when("/errorMsg", {
    templateUrl: "views/errorMsg.html",
    controller: "errorMsgCtrl",
   })
   .when("/about", {
    templateUrl: "views/about.html",
    controller: "aboutCtrl",
   })
   .otherwise({
    redirectTo: "/menu"
   });
 })
 .run(function($rootScope, $location, $timeout, $window, $http) { //Insert in the function definition the dependencies you need.

  $http({
   method: 'GET',
   url: 'configFile.txt',
   headers: {
    "Content-Type": "application/json"
   }
  }).success(function(data, status, headers, config) {

   sessionStorage["config"] = JSON.stringify(data);

  }).error(function(data, status, headers, config) {
   alert('failed response' + data);
  });

  // register listener to watch route changes
  $rootScope.$on("$locationChangeStart", function(e, currentLocation, previousLocation) {
   // event.preventDefault();
   console.log(previousLocation + "........." + new Date());
   var previousLocationStr = new Array();
   previousLocationStr = previousLocation.split(",");
   /// display elements  ///
   var siteID;
   window.scrollTo(0, 0);

   if (previousLocationStr[0] === 'file:///android_asset/www/index.html?payment=true') {
    if (sessionStorage.getItem("siteID") == null || sessionStorage.getItem("siteID") == "") {
     $location.path("/errorMsg");
    } else {
     siteID = "";
     siteID = sessionStorage.getItem("siteID");
     $location.path('/chat/' + siteID);
    }
   }

   if (previousLocationStr[0] === 'file:///android_asset/www/index.html?message=true') {
    var siteID = previousLocationStr[1];
    sessionStorage["storeid"] = siteID;
    $location.path('/chat/' + siteID);
   }
   if (previousLocationStr[0] === 'file:///android_asset/www/index.html?shop=true') {
    if (sessionStorage.getItem("siteID") == null || sessionStorage.getItem("siteID") == "") {
     $location.path("/errorMsg");
    } else {
     siteID = "";
     siteID = sessionStorage.getItem("siteID");
     $location.path('/chat/' + siteID);
    }
   }
   if (previousLocationStr[0] === 'file:///android_asset/www/index.html?about=true') {
    if (sessionStorage.getItem("siteID") == null || sessionStorage.getItem("siteID") == "") {
     $location.path("/errorMsg");
    } else {
     $location.path("/about");
    }
   }
  });
 });