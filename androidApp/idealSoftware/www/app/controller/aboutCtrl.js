angular.module("app")
    .controller("aboutCtrl", ["$scope", "$rootScope", "$window", "$location", "configDetailsProvider", "SharedService", function($scope, $rootScope, $window, $location, configDetailsProvider, SharedService) {

        $scope.clickToChat = function() {
            if (sessionStorage.getItem("siteID") == null || sessionStorage.getItem("siteID") == "") {
                $location.path("/errorMsg");
            } else {
                var siteID = sessionStorage.getItem("siteID");
                $location.path('/chat/' + siteID);
            }
        }
          // theme color replaced by config color
           document.documentElement.style.setProperty('--main-bg-color', configDetailsProvider.apiConnect.color);

           $rootScope.IsShopShow = configDetailsProvider.apiConnect.IsShopShow;

        if (configDetailsProvider.apiConnect.flag == false) {
            $(".ShopDisabled").css('pointer-events', 'none');
            $(".ShopDisabled").css('opacity', '0.6');
        }

        $(".main-logo").css("background", configDetailsProvider.apiConnect.image);
        $(".main-logo").css("background-size", "contain");

        $scope.about = function() {
            $location.path("/about");
        };

       //Redirect Payment
       $scope.redirect = function(mode) {
        sessionStorage.setItem("key", mode);
        $window.location.href = "paymentApp/index.html";
       };

    }]);