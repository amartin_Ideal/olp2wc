﻿angular.module("app")
    .controller("paymentHistoryCtrl", ["$scope", "$rootScope", "$location", "idealsApi", function($scope, $rootScope, $location, idealsApi) {

        try {
         $('#hamburger-inner-navbar').click(function () {
                        $("#inner-navbar").toggleClass("in");
                    });
            $rootScope.message = "";
            $scope.userFLName = sessionStorage.userFLName;
            // get payment_history api method response and bind data  on html table
            var listCards = JSON.parse(sessionStorage["pay_hist"] || "{}");
            var json = listCards.data.v1.payments;
            var paymentHIS = json;
            $scope.payHist = paymentHIS;

            $scope.localDateTime = function(date) {
                var date = new Date(date);
                return date;
            }

        } catch (e) {
            console.log(e);
        }
    }]);